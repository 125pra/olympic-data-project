  const csv = require('csvtojson');
  let convertToJson = async filePath => {
    let array = await csv().fromFile(filePath);
    return array;
  };

  let olympic_data = async () => {
    let nocRegions = await convertToJson("../data/noc_regions.csv");
    let athleteEvents = await convertToJson("../data/athlete_events.csv");
    return {
      nocRegions,
      athleteEvents
    }
  };

  module.exports = {
    olympic_data
  }