let fs = require('fs');
let olympic = require('./olympic');
let getData = require('./parseCSV');



getData.
getOlympicData().
then(async (event) => {
  let nocRegions = event.nocRegions;
  let athleteEvents = event.athleteEvents;
  let jsonData = {};

  let cityHosted = olympic.cityCount(athleteEvents);
  //  console.log(cityHosted);

  jsonData["cityHosted"] = cityHosted;

  let top10Country = olympic.countTop(athleteEvents,2000);
  // console.log(top10Country);
  jsonData["top10Country"] = top10Country;

  let melOrFemale = olympic.countGender(athleteEvents);
  //  console.log(melOrFemale);
  jsonData["melOrFemale"] = melOrFemale;

  let avrageAgeOfAthletes = olympic.averageAge(athleteEvents,"Boxing Men's Heavyweight");
  //  console.log(avrageAgeOfAthletes);

  jsonData["avrageAgeOfAthletes"] = avrageAgeOfAthletes;

  let noOfMedelsByIndia = olympic.medalCountForIndia(athleteEvents,"India");
  // console.log(noOfMedelsByIndia);

  jsonData["noOfmedalsByIndia"] = noOfMedelsByIndia



  fs.writeFile('../output/data.json',
    JSON.stringify(jsonData, null, 5),
    err => {
      if (err) {
        console.log(err);
      }
    }
  );

});