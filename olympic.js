//Question 1   "Number of times olympics hosted per City"

cityCount = (athleteEvents) => {

    let cityCountForEvent = athleteEvents.reduce((noOfEvents, singleData) => {
        if (!noOfEvents['ids'].has(singleData.Games)) {
            noOfEvents['ids'].add(singleData.Games);
            if (noOfEvents[singleData.City]) {
                noOfEvents[singleData.City]++;
            } else {
                noOfEvents[singleData.City] = 1;
            }
        }
        return noOfEvents
    }, {
        ids: new Set()
    });

    delete cityCountForEvent.ids;
    return cityCountForEvent;
}

// Question 2    "Counting top 10 Country with most medel after 2000"

countTop = (athleteEvents, limitYear) => {
    let countryMedal = athleteEvents.filter(filterByYear => filterByYear["Year"] > limitYear).filter(removeNa => removeNa["Medal"] !== "NA").reduce((noOfEvents, singleData) => {
        if (!noOfEvents[singleData.Team]) {
            noOfEvents[singleData.Team] = {};
            noOfEvents[singleData.Team]["total"] = 0;
        }
        if (noOfEvents[singleData.Team][singleData.Medal]) {
            noOfEvents[singleData.Team][singleData.Medal]++
            noOfEvents[singleData.Team]["total"]++
        } else {
            noOfEvents[singleData.Team][singleData.Medal] = 1;
            noOfEvents[singleData.Team]["total"]++
        }
        return noOfEvents;
    }, {})

    var arrayOfAllCountry = Object.entries(countryMedal);
    var sortedArrayOfMedalCount = arrayOfAllCountry.sort(function (a, b) {
        return b[1]["total"] - a[1]["total"];
    })
    return sortedArrayOfMedalCount.slice(0, 10);
}

// question 3   "M/F participation by decade"


countGender = (athleteEvents) => {

    let yearWiseGender = athleteEvents.reduce((noOfEvents, singleData) => {
        if (!noOfEvents[singleData.Year]) {
            noOfEvents[singleData.Year] = {};
        }
        if (!noOfEvents['ids'].has(singleData.ID)) {
            noOfEvents['ids'].add(singleData.ID);
            if (noOfEvents[singleData.Year][singleData.Sex]) {
                noOfEvents[singleData.Year][singleData.Sex]++
            } else {
                noOfEvents[singleData.Year][singleData.Sex] = 1;
            }
        }
        return noOfEvents;
    }, {
        ids: new Set()
    })
    delete yearWiseGender.ids
    var years = Object.entries(yearWiseGender)

    let genderCountPer_Decade = years.reduce((genderCountPerDecade, singleData) => {
        var decade = singleData[0].slice(0, 3) + "0"
        var decade = decade + "-" + decade.slice(0, 3) + "9";
        if (!genderCountPerDecade[decade]) {
            genderCountPerDecade[decade] = {};
            genderCountPerDecade[decade]["M"] = 0;
            genderCountPerDecade[decade]["F"] = 0;
        }
        genderCountPerDecade[decade]["M"] = genderCountPerDecade[decade]["M"] + Number(singleData[1]["M"]);
        genderCountPerDecade[decade]["F"] = genderCountPerDecade[decade]["F"] + Number(singleData[1]["F"]);
        return genderCountPerDecade;
    }, {})
    return genderCountPer_Decade;

}


// question 4   "Per season average age of athletes who participated in Boxing Men’s Heavyweight"

averageAge = (athleteEvents, event) => {
    var yearWithAge = athleteEvents.filter(choseEvent => choseEvent["Event"] === event).filter(removeNa => removeNa["Age"] !== "NA")
        .reduce((noOfEvents, singleData) => {
            let age = Number(singleData.Age);
            if (!noOfEvents[singleData.Year]) {
                noOfEvents[singleData.Year] = []
            } else {
                noOfEvents[singleData.Year].push(age);
            }
            return noOfEvents
        }, {})

    yearWithAge = Object.entries(yearWithAge)
    let yearWithAvgAGE = yearWithAge.reduce((year, singleYearWithAge) => {
        year[singleYearWithAge[0]] = (singleYearWithAge[1].reduce((a, b) => {
            return a + b
        }, 0) / singleYearWithAge[1].length).toFixed(2);
        return year;
    }, {})
    return yearWithAvgAGE;
}

//question 5  all medal winners from India per season 

medalCountForIndia = (athleteEvents, choseCuntry) => {

    return athleteEvents.filter(findCountry => findCountry["Team"] == choseCuntry).filter(removeNa => removeNa["Medal"] !== "NA")
        .reduce((noOfEvents, singleData) => {
            if (!noOfEvents[singleData.Year]) {
                noOfEvents[singleData.Year] = [];
            }
            if (noOfEvents[singleData.Year].indexOf(singleData.Name) < 0) {
                noOfEvents[singleData.Year].push(singleData.Name)
            }
            return noOfEvents;
        }, {})

}


module.exports = {
    cityCount,
    medalCountForIndia,
    averageAge,
    countGender,
    countTop
}